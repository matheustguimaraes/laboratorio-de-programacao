#include <stdio.h>

int main() {

    int M, note100, note50, note20, note10, note5, note2, note1, coins;
    int coin50, coin25, coin10, coin5, coin1;
    double N;

    scanf("%lf", &N);

    if (M > 1000001) {
        printf("Erro");
    } else
        N = N * 100;
    M = N / 100;
    N = N / 100;
    coins = ((N - M) * 100);

    if ((coins * 1000) % 10 == 9) {
        coins++;
    }

    note100 = M / 100;
    M %= 100;
    note50 = M / 50;
    M %= 50;
    note20 = M / 20;
    M %= 20;
    note10 = M / 10;
    M %= 10;
    note5 = M / 5;
    M %= 5;
    note2 = M / 2;
    M %= 2;

    note1 = M;
    coin50 = coins / 50;
    coins %= 50;
    coin25 = coins / 25;
    coins %= 25;
    coin10 = coins / 10;
    coins %= 10;
    coin5 = coins / 5;
    coins %= 5;
    coin1 = coins;

    printf("NOTAS:\n");
    printf("%d nota(s) de R$ 100.00\n", note100);
    printf("%d nota(s) de R$ 50.00\n", note50);
    printf("%d nota(s) de R$ 20.00\n", note20);
    printf("%d nota(s) de R$ 10.00\n", note10);
    printf("%d nota(s) de R$ 5.00\n", note5);
    printf("%d nota(s) de R$ 2.00\n", note2);


    printf("MOEDAS:\n");
    printf("%d moeda(s) de R$ 1.00\n", note1);
    printf("%d moeda(s) de R$ 0.50\n", coin50);
    printf("%d moeda(s) de R$ 0.25\n", coin25);
    printf("%d moeda(s) de R$ 0.10\n", coin10);
    printf("%d moeda(s) de R$ 0.05\n", coin5);
    printf("%d moeda(s) de R$ 0.01\n", coin1);

    return 0;
}

